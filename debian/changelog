ez-vcard (0.11.2+dfsg-2) unstable; urgency=medium

  * Team upload.
  * Ignore the dependency on xalan
  * Standards-Version updated to 4.7.0

 -- Emmanuel Bourg <ebourg@apache.org>  Mon, 28 Oct 2024 17:40:41 +0100

ez-vcard (0.11.2+dfsg-1) unstable; urgency=medium

  [ Mechtilde ]
  * [a4bd543] Reformat d/watch
  * [fbc617d] New upstream version 0.11.2+dfsg
  * [85eac57] Adapt patch to new version

 -- Mechtilde Stehmann <mechtilde@debian.org>  Mon, 16 Nov 2020 17:55:17 +0100

ez-vcard (0.11.1+dfsg-1) unstable; urgency=medium

  [ Mechtilde ]
  * [401e37b] Corrected d/watch to resolve uscan error
  * [db38744] Added dfsg to watch file
  * [ae582d7] Improved list of files to exclude and some more improvements
              in d/copyright
  * [b36e895] New upstream version 0.11.1+dfsg
  * [b6609ad] Bumped compat version to 13 and improved path to salsa
              in d/control
  * [93ec007] Removed +ds from d/watch
  * [b1cfd9c] Adapt patches to new versions
  * [8df56bc] added version to libjackson-core -java in d/control
  * [c0182fe] Removed useless licence

 -- Mechtilde Stehmann <mechtilde@debian.org>  Tue, 11 Nov 2020 14:26:01 +0100

ez-vcard (0.10.6+ds+dfsg-1) unstable; urgency=medium

  [ Mechtilde ]
  * [ef584c4] New upstream version 0.10.6+ds+dfsg
  * [c56f1ac] adapted patch to new upstream version
  * [a6f608b] adapted patch to new upstream version
  * [c0e99b4] Added d/u/metadata
  * [47d5562] Bump year in d/copyright
  * [a7d2e02] bumped standard version in d/control no changes needed
  * [df76e19] Added multi-arch to d/control
  * [382dfaf] corrected version identifier in d/maven.rules
  * [335c326] Added bug tracker in d/upstream/metadata

 -- Mechtilde Stehmann <mechtilde@debian.org>  Mon, 13 Apr 2020 22:02:09 +0200

ez-vcard (0.10.5+ds+dfsg.1-1) unstable; urgency=medium

  * Prepared for upload to unstable (Closes: #939607)

 -- Mechtilde Stehmann <mechtilde@debian.org>  Sat, 04 Jan 2020 20:36:34 +0100

ez-vcard (0.10.5+ds+dfsg.1-1~exp1) experimental; urgency=medium

  [ Mechtilde ]
  * [87b60da] Remove Javascript microcode and PDFs with non-free RFCs.
  * [3a0588d] New upstream version 0.10.5+ds+dfsg.1

 -- Mechtilde Stehmann <mechtilde@debian.org>  Sat, 04 Jan 2020 18:23:10 +0100

ez-vcard (0.10.5+ds-1~exp2) experimental; urgency=medium

  [ Mechtilde ]
  * [80a4185] Improved debian/copyright
  * [d1bc11e] Improved for lintian
  * [9c9f75d] Prepared for upload: debian/changelog

 -- Mechtilde Stehmann <mechtilde@debian.org>  Tue, 31 Dec 2019 08:38:03 +0100

ez-vcard (0.10.5+ds-1~exp1) experimental; urgency=medium

  [ Mechtilde ]
  * [b162a1d] New upstream version 0.10.5+ds
  * [b687515] Initial Debian release (Closes: #939607)
  * [0135afe] improved debian/watch
  * [8d4c624] removed whitespace from debian/copyright

 -- Mechtilde Stehmann <mechtilde@debian.org>  Wed, 02 Oct 2019 21:46:19 +0200
