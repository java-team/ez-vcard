Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: ez-vcard
Source: https://github.com/mangstadt/ez-vcard
Files-Excluded: src/main/javadoc/doc-files/shCore.js
 src/main/javadoc/ezvcard/doc-files/shCore.js
 src/main/javadoc/ezvcard/io/doc-files/shCore.js
 src/main/javadoc/ezvcard/parameter/doc-files/shCore.js
 src/main/javadoc/ezvcard/property/doc-files/shCore.js
 src/main/javadoc/ezvcard/util/doc-files/shCore.js
 src/main/javadoc/ezvcard/io/chain/doc-files/shCore.js
 src/main/javadoc/ezvcard/io/html/doc-files/shCore.js
 src/main/javadoc/ezvcard/io/json/doc-files/shCore.js
 src/main/javadoc/ezvcard/io/scribe/doc-files/shCore.js
 src/main/javadoc/ezvcard/io/text/doc-files/shCore.js
 src/main/javadoc/ezvcard/io/xml/doc-files/shCore.js
 docs/rfc2426-vcard3.pdf
 docs/rfc3966-tel-uri.pdf
 docs/rfc4770-impp.pdf
 docs/rfc5870-geo-uri.pdf
 docs/rfc6350-vcard4.pdf
 docs/rfc6351-xcard.pdf
 docs/rfc6473-kind-application.pdf
 docs/rfc6474-birth-death.pdf
 docs/rfc6715-oma.pdf
 docs/rfc6868-circumflex.pdf
 docs/rfc6869-kind-device.pdf
 docs/rfc7095-jcard.pdf
 docs/vcard-21.pdf
Comment: Remove Javascript microcode and PDFs with non-free RFCs. 

Files: *
Copyright: 2012-2020 Michael Angstadt
License:   BSD-2-Clause

Files:     src/main/javadoc/doc-files/shBrushJava.js
           src/main/javadoc/ezvcard/doc-files/shBrushJava.js
           src/main/javadoc/ezvcard/io/chain/doc-files/shBrushJava.js
           src/main/javadoc/ezvcard/io/doc-files/shBrushJava.js
           src/main/javadoc/ezvcard/io/html/doc-files/shBrushJava.js
           src/main/javadoc/ezvcard/io/json/doc-files/shBrushJava.js
           src/main/javadoc/ezvcard/io/scribe/doc-files/shBrushJava.js
           src/main/javadoc/ezvcard/io/text/doc-files/shBrushJava.js
           src/main/javadoc/ezvcard/io/xml/doc-files/shBrushJava.js
           src/main/javadoc/ezvcard/parameter/doc-files/shBrushJava.js
           src/main/javadoc/ezvcard/property/doc-files/shBrushJava.js
           src/main/javadoc/ezvcard/util/doc-files/shBrushJava.js
           src/main/javadoc/syntaxhighlighter.css
Copyright: 2004-2010 Alex Gorbatchev.
License:   MIT or GPL

Files:     src/test/java/ezvcard/io/json/JCardDeserializerTest.java
           src/test/java/ezvcard/io/json/JCardSerializerTest.java
           src/main/java/ezvcard/io/json/JCardRawWriter.java
           src/main/java/ezvcard/io/json/JCardPrettyPrinter.java
           src/main/java/ezvcard/io/json/JCardSerializer.java
Copyright: 2012-2018 Michael Angstadt, 
           Buddy Gorven
License:   BSD-2-Clause

Files:     src/main/java/ezvcard/parameter/ImageType.java
           src/main/java/ezvcard/parameter/SoundType.java
           src/main/java/ezvcard/parameter/KeyType.java
           src/main/java/ezvcard/VCardVersion.java
Copyright: 2011 George El-Haddad.
           2012-2018 Michael Angstadt
License:   BSD-2-Clause

Files:     debian/*
Copyright: 2019 -2020 Mechtilde Stehmann <mechtilde@debian.org>
License:   BSD-2-Clause

License:   BSD-2-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 .
 The views and conclusions contained in the software and documentation are those
 of the authors and should not be interpreted as representing official policies,
 either expressed or implied, of the FreeBSD Project.

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.

License: GPL
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL".
